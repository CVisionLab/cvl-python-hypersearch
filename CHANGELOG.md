# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## Unreleased
Add unreleased changes below. Do not remove this line.


## [v1.0.0] - 2021-04-05
Project license changed to MIT. See LICENSE file in the repository root.

### Added
* Documented project compatibility policy. The project will follow officially supported python version as defined in [Python Developer's Guide](https://devguide.python.org/#status-of-python-branches).

### Changed
* Dropped support for python3.4, python3.5 and added support for python3.9.

[v1.0.0]: https://gitlab.com/CVisionLab/cvl-python-hypersearch/-/compare/v0.6.0...v1.0.0


## [0.6.0] - 2020-02-13
### Changed
* Removed python2 support.
* Implemented CI.
* Implemented unit-tests.

[0.6.0]: https://gitlab.com/CVisionLab/cvl-python-hypersearch/-/compare/v0.5.0...v0.6.0