# CVL Python Hyperparams search

This repo contains python implementation of hyper-params search.

Package works correctly on python >= 3.6.

Full documentation can be found here: https://cvisionlab.gitlab.io/cvl-python-hypersearch/

## Installation
Use `pip install git+https://git@gitlab.com/CVisionLab/cvl-python-hypersearch.git` to install latest
version or `pip install git+https://git@gitlab.com/CVisionLab/cvl-python-hypersearch.git@<version>`
to install specific version.

## Compatibility policy (supported python versions)
This repository follows [official status of python branches](https://devguide.python.org/#status-of-python-branches).

This means:
- We aim to add support for new versions as they appear in the list.
- We drop support for outdated versions when they are removed from the list.