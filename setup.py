"""
Black-box hyper parameters optimization with multiple backend-s.

Optimizes target application as a whole, passing parameters via command line.
Application must output loss value in a parse-able way.
"""

from setuptools import setup, find_namespace_packages

setup(
    name='cvl-hypersearch',

    use_scm_version=True,

    author='Dmitri Lapin',
    author_email='lapin@cvisionlab.com',
    description=__doc__,

    packages=find_namespace_packages(include=['cvl.*']),

    python_requires='>=3.6',
    setup_requires=['setuptools_scm'],
    tests_require=['pytest', 'pytest-timeout'],
    install_requires=[
        'sympy',
        'urwid',
        'PyYAML',
        'numpy',
        ('cvl-utils @ git+https://gitlab.com/CVisionLab/cvl-python-utils.git')
    ],
    entry_points={
        'console_scripts': [
            'cvl-hypersearch = cvl.hypersearch.hyper_search:main'
        ]
    },
    classifiers=[
        'Programming Language :: Python :: 3',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
    ],
    zip_safe=False
)
