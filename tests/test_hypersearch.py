# -*- coding: UTF-8 -*-

"""Tests for blackbox hypersearch."""

from cvl.hypersearch import hparams


def test_hparams_creation():
    """Test how hparams object are created from string."""
    hparams_def = [
        ['name=--y', 'min=1', 'max=1000', 'transform=log'],
        ['name=--x', 'min=-11', 'max=11', 'transform=int'],
        ['name=--z', 'min=-12', 'max=12.3',
         'transform_in=x+1', 'transform_out=x-1']
    ]
    hyper_params = hparams.HyperParams.from_definitions(hparams_def)

    assert hyper_params['y'].fullname == 'y'
    assert hyper_params['y'].name == '--y'
    assert abs(hyper_params['y'].min - 1) < 1e-3
    assert abs(hyper_params['y'].max - 1000) < 1e-3

    assert hyper_params['x'].fullname == 'x'
    assert hyper_params['x'].name == '--x'
    assert abs(hyper_params['x'].min - (-11)) < 1e-3
    assert abs(hyper_params['x'].max - 11) < 1e-3

    assert hyper_params['z'].fullname == 'z'
    assert hyper_params['z'].name == '--z'
    assert abs(hyper_params['z'].min - (-12)) < 1e-3
    assert abs(hyper_params['z'].max - 12.3) < 1e-3

    search_space = hyper_params.search_space()
    assert abs(search_space['y'][0] - 0) < 1e-3
    assert abs(search_space['y'][1] - 3) < 1e-3
    assert abs(search_space['x'][0] - (-11)) < 1e-3
    assert abs(search_space['x'][1] - 11) < 1e-3
    assert abs(search_space['z'][0] - (-11)) < 1e-3
    assert abs(search_space['z'][1] - 13.3) < 1e-3

    random_point = hyper_params.random_sample()
    for param_name in ['x', 'y', 'z']:
        assert (hyper_params[param_name].min <=
                random_point[param_name] <=
                hyper_params[param_name].max)
