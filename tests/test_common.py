"""Tests for functions from common module."""

import re

from cvl.hypersearch import common


def test_sympy2fn():
    """Test creating python func from string."""
    expression = 'x ** 2 + 1'
    pfunc = common.sympy2fn(expression)
    assert abs(pfunc(x=2) - 5) <= 1e-4
    assert abs(pfunc(x=-2) - 5) <= 1e-4
    assert abs(pfunc(x=10) - 101) <= 1e-4


def test_flatting():
    """Test flatting map functions."""
    mapping = {
        'k1': 1,
        'k2': {'k11': 2.5, 'k22': 3.2},
        'k3': 2
    }
    flatten_mapping_gt = {
        'k1': 1,
        'k2/k11': 2.5,
        'k2/k22': 3.2,
        'k3': 2
    }
    all_keys_gt = ['k1', 'k2/k11', 'k2/k22', 'k3']
    all_vals_gt = [1, 2.5, 3.2, 2]

    all_keys = common.get_all_keys(mapping)
    all_vals = common.get_all_vals(mapping)
    flatten_mapping = common.get_flatten_mapping(mapping)
    restored_mapping = common.restore_mapping_hierarchy(flatten_mapping)

    assert all_keys == all_keys_gt
    assert all_vals == all_vals_gt
    assert flatten_mapping == flatten_mapping_gt
    assert restored_mapping == mapping


def test_objective_and_variance_extract():
    """Test objective and variance extraction from string."""
    regexes = [
        r'(?:Loss:\s*(?P<value>{float}))'
        r'(?:,\s*Variance:\s*(?P<variance>{float}))?',

        r'(?P<value>{float})'
        r'(?:\s+(?P<variance>{float}))?'
    ]
    regex_subs = {
        'float': r'(?:[-+]?(?:(?:(?:\d+(?:\.\d*)?|\.\d+)(?:e[-+]?\d+)?))|'
                 r'(?:inf))'
    }
    obj_transform = common.Transform('x + 3', 'x - 3')
    var_transform = common.Transform('x * 2.4', 'x / 2.4')
    examples = [
        [{'line': 'Loss: 3.14, Variance: 5.0', 'obj': 3.14, 'var': 5.0},
         {'line': 'Loss:3,Variance:6', 'obj': 3.0, 'var': 6.0},
         {'line': 'Loss:  3.14,  Variance:  5.0', 'obj': 3.14, 'var': 5.0}],

        [{'line': '3.14 5.0', 'obj': 3.14, 'var': 5.0},
         {'line': '3.14   5.0', 'obj': 3.14, 'var': 5.0}]
    ]

    for regex, reg_examples in zip(regexes, examples):
        regex = regex.format(**regex_subs)
        regex = re.compile(regex)
        objective = common.Objective(regex, obj_transform, var_transform)
        for ex in reg_examples:
            obj, var, sspace_obj, sspace_var = objective.parse(ex['line'], 0)
            assert abs(obj - ex['obj']) < 1e-3
            assert abs(var - ex['var']) < 1e-3
            assert abs(obj_transform.out(x=sspace_obj) - ex['obj']) < 1e-3
            assert abs(var_transform.out(x=sspace_var) - ex['var']) < 1e-3
