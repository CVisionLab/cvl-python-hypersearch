# Tutorial

This framework allows to optimize any black-box application.
It is assumed that app prints objective function value and variance in some
format. Framework is able to extract them and suggest next point to
investigate. Black-box script has several hyper-params which framework tries
to find best values for.

Framework is able to run different optimizers.


## [MOE](https://github.com/Yelp/MOE) optimizer using configuration
1. Run MOE server using command:  
   `docker run -p 6543:6543 yelpmoe/latest`

2. Run `cvl-hypersearch --help-backends` to check if moe backend is active.  
   ```
   moe: MOE optimization engine
   MOE optimization engine requires running MOE server. Please follow official instructions to setup MOE server: http://yelp.github.io/MOE/install.html

   DISABLED: Install clientMOE package and scipy using `pip`:
   pip3 install 'clientmoe @ https://github.com/RokoMijic/clientMOE/archive/master.zip' scipy

   The clientMOE package above is python3 update of the original clientMOE package available on PyPI.

   Moe backend options:
     --moe-host MOE_HOST   MOE server host (default: 127.0.0.1)
     --moe-port MOE_PORT   MOE server port (default: 6543)
     --moe-covariance-type MOE_COVARIANCE_TYPE
                           covariance type (default: square_exponential)
     --moe-hyperparameters MOE_HYPERPARAMETERS [MOE_HYPERPARAMETERS ...]
                           covariance hyper parameters (default: None)

   ```

3. Install python3 [client for moe](https://github.com/RokoMijic/clientMOE) 
   and scipy packages as shown in the output above if the backed in disabled 
   (as shown above).  
   `pip3 install 'clientmoe @ https://github.com/RokoMijic/clientMOE/archive/master.zip' scipy`

4. Run `testapp` with default config:  
   ```
   python -m cvl.hypersearch.hyper_search -vv --backend moe --steps 100 
   --num-workers 2 --sort-history --hyper-param name=--x min=-10 max=10 
   --hyper-param name=--y min=-10 max=10  -- python -m cvl.hypersearch.testapp 
   --num-iters 0 --variance 0.0001
   ```

   By default, `testapp` prints value of the function:
   `(x - 1) ** 2 + (y - 1) ** 2`. After processing, you should see that optimal
   values for `x` and `y` are close to `1.0` which is correct.