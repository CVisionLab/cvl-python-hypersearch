cvl-hypersearch
===============

Black-box hyper-parameters optimization.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   README
   tutorial
   api/cvl.hypersearch


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
