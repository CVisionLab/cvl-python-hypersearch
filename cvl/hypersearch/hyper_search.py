#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Hyper-parameters optimization application."""

import argparse
import re
import sys
import collections as coll
import warnings

from cvl.utils import timer

from . import backends
from .hparams import HPARAMDEF, HyperParams
from .common import Objective
from .app import HyperSearchApp
from .cmdline_builder import CmdLineBuilder
from .version import __version__


def show_transforms_help(builtin):
    """
    Display help about transformations.

    Parameters
    ----------
    builtin: dict
        Dictionary of built-in transformations.
    """
    print('Transforms define transformations of parameters before '
          'passing them to optimizer and after obtaining next sample '
          'point from optimizer.\n'
          'Each transform defines input transformation (in) that transforms '
          'parameters to values that optimizer will use and output '
          'transformation (out) that transforms values got from optimizer '
          'to actual parameter values')

    print()
    print('Available transforms:')
    for _name, _tr in builtin.items():
        print('\t{0}: {1.description}'.format(_name, _tr))
    print()
    print('Custom transforms can be defined with keywords transform_in and '
          'transform_out as sympy expressions depending on free variable `x`')


def show_hyper_param_help(builtin):
    """
    Display help about hyper-params definition.

    Parameters
    ----------
    builtin: dict
        Dictionary of allowed hyper parameter keywords.
    """
    print('The `--hyper-param` option defines hyper parameter to optimize. '
          'Option can be used several times and each usage defines single '
          'hyper parameter.')
    print('Each hyper parameter definition accepts several `keyword=value` '
          'pair. Three of them are required:')
    print('\tname: argument name as at it will be passed to binary')
    print('\t\twith all required dashes, may require quoting')
    print('\tmin: minimal parameter value')
    print('\tmax: maximal parameter value')
    print()
    print('All supported keywords:')
    for _name, _value in builtin.items():
        if isinstance(_value, list):
            print('\t{0}: {1}'.format(_name, _value))
        elif isinstance(_value, dict):
            print('\t{0}: {1}'.format(_name, _value.keys()))
        else:
            print('\t{0}: {1}'.format(_name, _value.__name__))


def show_pattern_subs_help(subs):
    """
    Display help about pattern substitution.

    Parameters
    ----------
    subs: dict
        Dictionary of defined substitutions.
    """
    print('Pattern defined via command line can use following substitutions:')
    for name, value in subs.items():
        print('\t{0}: {1}'.format(name, value))


def show_backends_help(backends_list):
    """
    Display help on backend choosing.

    Parameters
    ----------
    backends_list: list
        List of registered backends.
    """
    print('Registered backends:\n')
    for _backend in backends_list.values():
        print('=' * 80, '\n')
        print('{backend.name}: {backend.title}'.format(backend=_backend))
        if _backend.description:
            print(_backend.description)
        if _backend.type.enabled() is not True:
            print('\nDISABLED:', _backend.type.enabled())
        print()
        _parser = argparse.ArgumentParser(
            add_help=False,
            usage=argparse.SUPPRESS,
            formatter_class=argparse.ArgumentDefaultsHelpFormatter
        )
        _agroup = _parser.add_argument_group(
            title='{0} backend options'.format(_backend.name.capitalize()))
        _backend.type.populate_parser(_agroup)
        _parser.print_help()
        print()


def show_format_arg_help(substitutions):
    """
    Show help for --format-arg parameter.

    Parameters
    ----------
    substitutions: dict
        Dictionary of available substitutions.
    """
    print('Format string used with --format-arg argument can use the '
          'following substitutions (names to use with standard python '
          'format string syntax):\n')
    for name, value in substitutions.items():
        print('\t{0}: {1}'.format(name, value.help))
    print()
    print('Example usage : --format-arg " --name" "{name}" '
          '--format-arg " --worker" "{worker}"')


def write_multi(text, files, eol=True):
    """
    Write the same text to the multiple streams.

    Parameters
    ----------
    text: str
        Text to write.
    files: list
        List of file streams.
    eol: bool, optional
        Add EOL if True.
    """
    for file in files:
        if file is not None:
            file.write(text)
            if eol:
                file.write('\n')


def save_config(args, parser, backend_parser, stream):
    """
    Save config corresponding to passed arguments in YAML format.

    Saves only non-default options

    Parameters
    ----------
    args: argparse.Namespace
        Command-line arguments.
    parser: argparse.ArgumentParser
        Application parser.
    backend_parser: argparse.ArgumentParser
        Optimization backend parser.
    stream: file
        File stream to write in.
    """
    import yaml
    _mapping_tag = yaml.resolver.BaseResolver.DEFAULT_MAPPING_TAG

    def dict_representer(dumper, data):
        return dumper.represent_dict(data.items())

    def dict_constructor(loader, node):
        return coll.OrderedDict(loader.construct_pairs(node))

    yaml.add_representer(coll.OrderedDict, dict_representer)
    yaml.add_constructor(_mapping_tag, dict_constructor)

    ignored = {'config', 'dry_run', 'hyper_param', 'save_config', 'cmdargs'}
    as_dict = {'format_arg', 'env'}
    config = coll.OrderedDict()
    # backend options
    backend = coll.OrderedDict()

    # save options ignoring defaults
    for argname, argvalue in vars(args).items():
        # skip ignored
        if argname in ignored:
            continue
        # skip default values
        if argvalue == parser.get_default(argname):
            continue
        savename = argname.replace('_', '-')
        # skip backend options
        if backend_parser.get_default(argname) is not None:
            backend[savename] = argvalue
            continue
        if argname in as_dict:
            argvalue = {name: value for name, value in argvalue}
        config[savename] = argvalue

    config['backend'] = args.backend
    config['backend-options'] = backend
    config['exec'] = args.cmdargs

    # save hyper parameters
    hparams = coll.OrderedDict()
    for paramdef in args.hyper_param:
        param = coll.OrderedDict()
        param_name = None
        for name, value in [p.split('=') for p in paramdef]:
            if name == 'name':
                param_name = value
            else:
                param[name] = value
        if param_name is not None:
            hparams[param_name] = param

    config['hyper-params'] = hparams

    yaml.dump(config, stream, default_flow_style=False)


def load_config(file_stream):
    """
    Load run configuration from saved YAML file.

    Parameters
    ----------
    file_stream: file
        Config in ``.yaml`` file stream.

    Returns
    -------
    dict
        Run configuration dict.
    """
    import yaml
    _mapping_tag = yaml.resolver.BaseResolver.DEFAULT_MAPPING_TAG

    def dict_representer(dumper, data):
        return dumper.represent_dict(data.items())

    def dict_constructor(loader, node):
        return coll.OrderedDict(loader.construct_pairs(node))

    yaml.add_representer(coll.OrderedDict, dict_representer)
    yaml.add_constructor(_mapping_tag, dict_constructor)
    config = yaml.load(file_stream)
    from_dict = {'format-arg', 'env'}

    # build hyper parameters definition list
    hparams = []
    for hpname in config['hyper-params']:
        hparam = []
        hparam.append('name={name}'.format(name=hpname))
        hparam_data = config['hyper-params'][hpname]
        for keyname in hparam_data:
            hparam.append('{name}={value}'.format(
                name=keyname, value=hparam_data[keyname]))
        hparams.append(hparam)
    config['hyper-params'] = hparams

    for cname in from_dict:
        if cname not in config:
            continue
        config[cname] = [[n, v] for n, v in config[cname].items()]

    return config


def merge_config(parser, args, config):
    """
    Merge config into arguments preserving passed (non-def) values.

    Parameters
    ----------
    parser: argparse.ArgumentParser
        Application command-line args parser.
    args: argparse.Namespace
        Application command-line args.
    config: dict
        Loaded from ``.yaml`` file configuration.
    """
    ignore = ['exec', 'backend', 'backend-options', 'hyper-params']
    for argname in config:
        if argname in ignore:
            continue
        nsname = argname.replace('-', '_')
        if getattr(args, nsname, None) == parser.get_default(nsname):
            setattr(args, nsname, config[argname])
        else:
            print('Warning: Overriding saved value for argument {argname} '
                  'from (saved) value {saved} to passed (not default) value '
                  '{passed}').format(argname=argname, saved=config[argname],
                                     passed=getattr(args, nsname, None))


def main():
    """Application entry point."""
    backends_list = backends.list_backends()
    enabled_backends = [backend.name for backend in backends_list.values()
                        if backend.type.enabled() is True]

    if len(enabled_backends) == 0:
        print('All backends are disabled')
        print()
        show_backends_help(backends_list)
        return

    bootstrap_parser = argparse.ArgumentParser(add_help=False)
    bootstrap_parser.add_argument('--help-transforms', action='store_true',
                                  default=argparse.SUPPRESS,
                                  help='Display available transforms.')
    bootstrap_parser.add_argument('--help-hyper-param', action='store_true',
                                  default=argparse.SUPPRESS,
                                  help='Display help on hyper parameters '
                                       'definition.')
    bootstrap_parser.add_argument('--help-format-arg', action='store_true',
                                  default=argparse.SUPPRESS,
                                  help='Display help on --format-arg '
                                       'parameter.')
    bootstrap_parser.add_argument('--help-pattern-subs', action='store_true',
                                  default=argparse.SUPPRESS,
                                  help='Display help on pattern '
                                       'substitutions.')
    bootstrap_parser.add_argument('--help-backends', action='store_true',
                                  default=argparse.SUPPRESS,
                                  help='Display list of available backends.')
    bootstrap_parser.add_argument('--help', '-h', action='store_true',
                                  default=argparse.SUPPRESS,
                                  help='Show this help message and exit.')
    bootstrap_parser.add_argument('--backend',
                                  choices=enabled_backends,
                                  default=enabled_backends[0],
                                  help='Optimization backend.')
    bootstrap_parser.add_argument('--config', type=argparse.FileType('r'),
                                  help='Load run configuration from '
                                       'this YAML-file.')

    bstrap_args = bootstrap_parser.parse_known_args()[0]

    use_config = bstrap_args.config is not None

    loaded_config = None
    if use_config:
        loaded_config = load_config(bstrap_args.config)
        if bstrap_args.backend == bootstrap_parser.get_default('backend'):
            bstrap_args.backend = loaded_config['backend']
        else:
            warnings.warn('Overriding backend with passed (not default)')

    # setup backend parser
    backend_parser = argparse.ArgumentParser(add_help=False)
    agroup = backend_parser.add_argument_group(
        title='{0} backend options'.format(
            backends_list[bstrap_args.backend].name.capitalize())
    )
    backends_list[bstrap_args.backend].type.populate_parser(agroup)

    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        parents=[bootstrap_parser, backend_parser],
        conflict_handler='resolve',
        fromfile_prefix_chars='@'
    )
    parser.add_argument(
        '--version', '-V', action='version',
        version=f'%(prog)s {__version__}'
    )
    parser.add_argument('--dry-run', '-n', action='store_true',
                        help='Print command instead of executing it.')
    parser.add_argument('--verbose', '-v', action='count', default=0,
                        help='Increase verbosity (up to 3 times).')
    parser.add_argument('--save-log', type=str,
                        help='Save execution history.')
    parser.add_argument('--save-config', type=argparse.FileType('w'),
                        help='Save run configuration to YAML file.')
    parser.add_argument('--history', type=str,
                        help='Use this file to save experiment history to '
                             'or to load if already exists and not empty.')
    parser.add_argument('--steps', '-s', type=int, default=100,
                        help='Number of optimization steps to run.')
    parser.add_argument('--num-workers', '-p', '-j', type=int, default=1,
                        help='Number of parallel workers.')
    parser.add_argument('--sort-history', action='store_true',
                        help='Sort experiment history (displayed starting '
                             'from the first verbosity level and in log file)')
    parser.add_argument('--objective-pattern', '-p',
                        default=r'(?P<value>{float})'
                                r'(?:\s+(?P<variance>{float}))?',
                        help='Pattern to capture value and variance from exec '
                             'output (default captures two space separated '
                             'float numbers, custom patterns must define '
                             'named groups "value" and "variance"); '
                             'pattern is a subject to python`s str.format() '
                             'call, so all curved braces ({}) must be '
                             'properly escaped; pattern can use substitutions,'
                             'use --help-pattern-subs to list available '
                             'substitutions.')
    parser.add_argument('--assume-inf', action='store_true',
                        help='Assume objective is infinite if not '
                             'found in output.')
    parser.add_argument('--objective-transform',
                        help='Objective value transform as sympy expression; '
                             'expression may use one or two free variables - '
                             '`x` or `x` and `t` (time of optimization run).')
    parser.add_argument('--variance-transform',
                        help='Variance value transform as sympy expression; '
                             'expression must use one free variable `x`.')
    parser.add_argument('--format-arg', nargs=2, action='append',
                        help='Allows passing arguments to the binary that '
                             'contain generated information '
                             '(like unique name); the first agument is an '
                             'argument name with all the required hyphens '
                             '(you may need to quote it), the second '
                             'argument is a format string '
                             '(use --help-format-arg to list available '
                             'substitutions).')
    parser.add_argument('--format-arg-pos', default='after-exec',
                        choices=['after-exec', 'after-fixed-args',
                                 'after-hyper-args'],
                        help='Position of arguments added with --format-arg '
                             '(after executable name, after fixed '
                             'arguments, or after hyper parameter based '
                             'arguments).')
    parser.add_argument('--env', nargs=2, action='append',
                        help='Set environment variables for running '
                             'processes; may use the same substitutions as '
                             '--format-arg; the first argument is a variable '
                             'name the second is value.')
    parser.add_argument('--hyper-param', nargs='+', action='append',
                        required=not use_config,
                        help='Define hyper parameter for optimization; '
                             'accepts variable number of arguments, '
                             'each argument has a form of keyword=value '
                             'pair; common keywords: '
                             'name (argument name), '
                             'min (minimum value), '
                             'max (maximum value), '
                             'start (value to start from) and '
                             'transform (use --help-transforms to '
                             'list supported); required keywords are name, '
                             'min and max; use --help-hyper-param to list '
                             'all available keywords.')
    parser.add_argument('cmdargs', nargs=('*' if use_config else '+'),
                        help='Binary and additional command line arguments '
                             'to pass to it exec.')

    regex_subs = {
        'float': r'(?:[-+]?(?:(?:(?:\d+(?:\.\d*)?|\.\d+)(?:e[-+]?\d+)?))|'
                 r'(?:inf))'
    }

    if 'help_transforms' in bstrap_args:
        show_transforms_help(HPARAMDEF['transform'])
        return

    if 'help_hyper_param' in bstrap_args:
        show_hyper_param_help(HPARAMDEF)
        return

    if 'help_format_arg' in bstrap_args:
        show_format_arg_help(HyperSearchApp.FORMAT_SUBSTITUTIONS)
        return

    if 'help_pattern_subs' in bstrap_args:
        show_pattern_subs_help(regex_subs)
        return

    if 'help_backends' in bstrap_args:
        show_backends_help(backends_list)
        return

    if 'help' in bstrap_args:
        parser.print_help()
        return

    args = parser.parse_args()

    # populate args from saved config
    if use_config:
        if bstrap_args.backend == bootstrap_parser.get_default('backend'):
            merge_config(parser, args, loaded_config['backend-options'])

        merge_config(parser, args, loaded_config)
        if len(args.cmdargs) > 0:
            warnings.warn('Overriding saved cmdargs.')
        else:
            args.cmdargs = loaded_config['exec']
        if args.hyper_param is not None:
            warnings.warn('Overriding saved hyper parameters definition')
        else:
            args.hyper_param = loaded_config['hyper-params']

        print('Efective config:\n')
        save_config(args, parser, backend_parser, sys.stdout)
        response = input('Continue? [Y/n] ')
        if response not in ['y', 'Y', '']:
            return

    try:
        hparams = HyperParams.from_definitions(args.hyper_param)
    except RuntimeError as e:
        print('Failed to parse hyper parameter definition:\n{0}'.format(e))
        return

    if args.save_config is not None:
        save_config(args, parser, backend_parser, args.save_config)

    sample_point = hparams.start_sample()
    objective_pattern = args.objective_pattern.format(**regex_subs)
    objective_re = re.compile(objective_pattern, re.VERBOSE | re.IGNORECASE)

    objective = Objective(
        objective_re,
        args.objective_transform,
        args.variance_transform,
        assume_inf=args.assume_inf)

    if args.format_arg_pos == 'after-exec':
        order = ['binary', 'format', 'fixed', 'hparams']
    elif args.format_arg_pos == 'after-fixed-args':
        order = ['binary', 'fixed', 'format', 'hparams']
    else:
        order = ['binary', 'fixed', 'hparams', 'format']

    cmdline_builder = CmdLineBuilder(
        hparams, args.cmdargs, args.format_arg, order)

    if args.dry_run:
        print('Optimizing:', args.cmdargs[0])
        print('Optimization steps:', args.steps)
        print('Static params:', ' '.join(args.cmdargs[1:]))
        print('Hyper params:')
        sspace_point = hparams.transform(sample_point)
        for name in sample_point:
            print(
                ('\t{hp.name} ∈ [{hp.min}, {hp.max}] → '
                 '{value} ({value_opt})').format(
                    value=sample_point[name],
                    value_opt=sspace_point[name],
                    hp=hparams[name]
                )
            )

        format_values = {
            name: value.fn() for name, value
            in HyperSearchApp.FORMAT_SUBSTITUTIONS.items()
            if value.fn is not None
        }
        cmdline = cmdline_builder.build(sample_point, format_values)

        print(' '.join(cmdline))
        return

    backend = backends_list[args.backend].type(hparams.search_space(), args)

    if args.save_log is not None:
        args.save_log = open(args.save_log, 'w')

    # run application

    app = HyperSearchApp(
        num_workers=args.num_workers,
        total_steps=args.steps,
        hparams=hparams,
        verbose=args.verbose,
        objective=objective,
        cmdline_builder=cmdline_builder,
        backend=backend,
        logfile=args.save_log,
        env=args.env,
        hist_file=args.history
    )

    app.run()

    if len(app.run_data_samples) == 0:
        return

    indices = range(len(app.run_data_samples))
    sorted_indices = sorted(
        indices,
        key=lambda idx: app.run_data_samples[idx].sspace_objective
    )
    if args.sort_history:
        indices = sorted_indices

    print()
    print(
        ('Best objective value {0.objective} ({0.sspace_objective}, {1}) '
         'with variance {0.variance} was sampled at point:').format(
            app.run_data_samples[sorted_indices[0]],
            timer.format_time(app.run_data_samples[sorted_indices[0]].time, 2)
        )
    )
    for name in hparams.keys():
        print('\t{0}: {1}'.format(
            hparams[name].name,
            app.run_data_samples[sorted_indices[0]].sample_point[name])
        )
    print()
    if args.verbose > 1 or args.save_log is not None:
        stdout = sys.stdout if args.verbose > 1 else None
        outputs = [stdout, args.save_log]
        write_multi('Experiment history:', outputs)
        fields = coll.OrderedDict([
            ('Step', lambda x: '{0}'.format(x.step)),
            ('Objective', lambda x: '{0:.15g}'.format(x.objective)),
            ('Variance', lambda x: '{0:.15g}'.format(x.variance)),
            ('Time', lambda x: timer.format_time(x.time, 2)),
            ('Time (sec)', lambda x: '{0}'.format(x.time)),
            ('Objective (opt)', lambda x: '{0:.15g}'.format(
                x.sspace_objective)),
            ('Point', lambda x: '{0}'.format(
                [x.sample_point.get(name, None) for name in hparams.keys()])),
            ('Point (opt)', lambda x: '{0}'.format(
                [x.sspace_point.get(name, None) for name in hparams.keys()]))
        ])
        formatted = []
        lengths = [len(field) for field in fields.keys()]
        for epoint in (app.run_data_samples[idx] for idx in indices):
            fmt_fields = [
                formatter(epoint) for fname, formatter
                in fields.items()
            ]
            lengths = [max(l, len(s)) for l, s in zip(lengths, fmt_fields)]
            formatted.append(fmt_fields)
        # print header
        for fname, flen in zip(fields.keys(), lengths):
            write_multi('{0:{1}}'.format(fname, flen + 2), outputs, False)
        write_multi('', outputs)
        # print values
        for fmt_fields in formatted:
            for fstr, flen in zip(fmt_fields, lengths):
                write_multi('{0:{1}}'.format(fstr, flen + 2), outputs, False)
            write_multi('', outputs)
        write_multi('', outputs)


if __name__ == '__main__':
    main()
