# -*- coding: utf-8 -*-

"""One hyper-param, collection of hyper-params and general def implement."""

import numpy as np
import re
import math
import collections as coll

from .common import Transform, SamplePoint
from copy import deepcopy


#: General hyper-param definition.
#: Contains allowed keys and their values, pre-processing or mapping.
HPARAMDEF = {
    'name': str,
    'min': float,
    'max': float,
    'transform': {
        'log': Transform(
            inp=lambda x: math.log10(x),
            out=lambda x: math.pow(10, x),
            description='log10 transform: in=log10(x), out=pow(10, x)'
        ),
        'int': Transform(
            inp=lambda x: x,
            out=lambda x: int(round(x)),
            description='int transform: in=x, out=int(round(x))'
        )
    },
    'transform_in': str,
    'transform_out': str,
    'start': float
}


class HParam(object):
    """Single hyper-parameter definition."""

    def __init__(self, name, min, max,
                 start=None, transform=None, parent=None):
        """
        Create single hyper-param instance.

        Parameters
        ----------
        name: str
            Name of the hyper-param.
        min: float
            Min possible val for the hyper-param.
        max: float
            Max possible val for the hyper-param.
        start: float, optional
            Start value for the hyper-param.
        transform: Transform, optional
            Hyper-param transformation object.
        parent: str, optional
            Parent hyper-param name.
        """
        self._name = name
        self._min = min
        self._max = max
        self._transform = transform or Transform(description='Identity')
        self._start = start or self.random_sample()
        self._parent = parent
        self._fullname = self._name.strip('- ')

    def random_sample(self):
        """
        Generate random sample of this hyper-param.

        Returns
        -------
        float
            Random (uniform) sample from hyper parameter space.
        """
        return self._transform.out(
            x=np.random.uniform(self._transform.inp(x=self._min),
                                self._transform.inp(x=self._max))
        )

    @property
    def fullname(self):
        """str: Get hyper-param fullname."""
        return self._fullname

    @property
    def name(self):
        """str: Get hyper-param name."""
        return self._name

    @property
    def min(self):
        """float: Get hyper-param min value."""
        return self._min

    @property
    def max(self):
        """float: Get hyper-param max value."""
        return self._max

    @property
    def transform(self):
        """Transform: Get hyper-param transformation obj."""
        return self._transform

    @property
    def start(self):
        """float: Get hyper-param start value."""
        return self._start

    @property
    def parent(self):
        """HParam: Get hyper-param parent hyper-param name."""
        return self._parent


class HyperParams(coll.OrderedDict):
    """Hyper-params collection."""

    def random_sample(self):
        """
        Generate hyper-params random sample.

        Returns
        -------
        SamplePoint
            Hyper-params random point.
        """
        # TODO: add support for structured search space
        return SamplePoint([(name, hp.random_sample()) for name, hp
                            in self.items()])

    def start_sample(self):
        """
        Get hyper-params start sample.

        Returns
        -------
        SamplePoint
            Hyper-params start point.
        """
        # TODO: add support for structured search space
        return SamplePoint([(name, hp.start) for name, hp in self.items()])

    def search_space(self):
        """
        Get hyper-params search space.

        Returns
        -------
        dict
            Hyper-params search space.
        """
        # TODO: add support for structured search space
        return {
            name: [hp.transform.inp(x=hp.min), hp.transform.inp(x=hp.max)]
            for name, hp in self.items()
        }

    def transform(self, sample_point, inverse=False):
        """
        Apply transform to the passed hyper-params sample point.

        Performs transformation to / from search space.

        Parameters
        ----------
        sample_point: SamplePoint
            Some hyper-params point.
        inverse: bool, optional
            Apply inverse transform if True.

        Returns
        -------
        SamplePoint
            Transformed hyper-params point.
        """
        transformed = deepcopy(sample_point)
        for name, value in transformed.items():
            if inverse:
                transformed[name] = self[name].transform.out(x=value)
            else:
                transformed[name] = self[name].transform.inp(x=value)
        return transformed

    @classmethod
    def from_definitions(cls, definitions):
        """
        Create hyper-params collection from list of definitions.

        Parameters
        ----------
        definitions: list
            List of sub-lists. Each sub-list contains strings like:
            ``keyword=value``. In other words, outer list corresponds to
            different hyper-params, inner to keywords of single
            hyper-param.

        Returns
        -------
        HyperParams
            New instance of Hyper-params collection.

        Raises
        -------
        RuntimeError: If passed only one of:
            ``transform_in`` or ``transform_out``.
        """
        hparams = cls()
        params = []
        # TODO: add support for structured search space
        required_keys = {'name', 'min', 'max'}
        for arglist in definitions:
            arginfo = cls._parse_hparam(arglist, HPARAMDEF, False)
            if not required_keys.issubset(arginfo.keys()):
                raise RuntimeError(
                    ('One or several of the required keys are missing: '
                     '{0}').format(' '.join(arglist)))
            if (
                    ('transform_in' in arginfo and
                     'transform_out' not in arginfo) or
                    ('transform_out' in arginfo and
                     'transform_in' not in arginfo)):
                raise RuntimeError('Both transform_in and transform_out must '
                                   'be specified at the same time: {0}'.format(
                                       ' '.join(arglist)))

            if 'transform_in' in arginfo and 'transform_out' in arginfo:
                arginfo['transform'] = Transform(
                    inp=arginfo['transform_in'],
                    out=arginfo['transform_out'],
                    description='in:{transform_in}/'
                                'out={transform_out}'.format(**arginfo))
                del arginfo['transform_in'], arginfo['transform_out']
            params.append(HParam(**arginfo))
        # TODO: link parents
        for param in params:
            hparams[param.fullname] = param
        return hparams

    @staticmethod
    def _parse_hparam(arglist, allowed_keys=None, allow_bool=True):
        """
        Parse one hyper-param definition.

        Parameters
        ----------
        arglist: list
            List of strings like `keyword=value`.
        allowed_keys: dict, optional
            Allowed keywords storage. Structure is
            `allowed_keys[some_key] = {Sequence, Mapping, Callable}`. If
            `allowed_keys[some_key]` is `Callable`, then it will be called
            with `value` from corresponding string. `Mapping` will map.
            `Sequence` define all possible choices for `value`.
        allow_bool: bool, optional
            If `True` then it's possible to use keyword without value.

        Returns
        -------
        dict
            Single hyper-param definition with all necessary keys for it.

        Raises
        -------
        RuntimeError: Failed to use `allowed_keys`,
            `allow_bool` is `False, but it was passed keyword without
            value.
        TypeError: Illegal type in `allowed_keys` values.
        """
        info = {}
        for arg in arglist:
            match = re.match(r'^\s*(?P<keyword>\w+)=(?P<value>.+)$', arg)
            if match is not None:
                if allowed_keys is not None:
                    keyword = match.group('keyword')
                    value = match.group('value')
                    if keyword not in allowed_keys:
                        raise RuntimeError(
                            'Keyword {0} is not understood'.format(
                                keyword)
                        )
                    if isinstance(allowed_keys[keyword], coll.Callable):
                        info[keyword] = allowed_keys[keyword](value)
                    elif isinstance(allowed_keys[keyword], coll.Mapping):
                        if value not in allowed_keys[keyword]:
                            raise RuntimeError(
                                'Keyword {0} accepts only following values: '
                                '{1}'.format(keyword,
                                             allowed_keys[keyword].keys())
                            )
                        info[keyword] = allowed_keys[keyword][value]
                    elif isinstance(allowed_keys[keyword], coll.Sequence):
                        if value not in allowed_keys[keyword]:
                            raise RuntimeError(
                                'Keyword {0} accepts only following values: '
                                '{1}'.format(keyword, allowed_keys[keyword])
                            )
                        info[keyword] = value
                    else:
                        raise TypeError('allowed_keys argument must be either '
                                        'callable, sequence or mapping')
                else:
                    info[match.group('keyword')] = match.group('value')
            else:
                if not allow_bool:
                    raise RuntimeError(
                        'Keyword {0} requires value'.format(arg))
                info[arg.lstrip()] = True
        return info
