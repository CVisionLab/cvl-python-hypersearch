# -*- coding: utf-8 -*-
#
#    Copyright (C) 2010  aszlig
#    Copyright (C) 2011  Ian Ward
#
#    This library is free software; you can redistribute it and/or
#    modify it under the terms of the GNU Lesser General Public
#    License as published by the Free Software Foundation; either
#    version 2.1 of the License, or (at your option) any later version.
#
#    This library is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#    Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public
#    License along with this library; if not, write to the Free Software
#    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# Urwid web site: http://excess.org/urwid/

"""Terminal emulator widget."""

from urwid.widget import Widget
from urwid.display_common import RealTerminal
from urwid.vterm import TermModes, TermCanvas


class DTerminal(Widget):
    """
    Terminal emulator widget.

    Attributes
    ----------
    has_focus : bool
        Current focus state.
    height : int
        Terminal height.
    keygrab : bool
        If terminal should grab keys.
    last_key : str
        The last pressed key.
    old_tios : bool
        Represents terminal type.
    response_buffer : list
        Response buffer.
    signals : list
        Supported signals.
    term : canvas
        Terminal widget canvas.
    term_modes : TermModes
        Terminal modes.
    terminated : bool
        If terminal finished.
    width : int
        Terminal width.
    """

    def __init__(self):
        """Init terminal instance."""
        self.__super.__init__()

        self._selectable = False
        self._sizing = frozenset(['box'])
        self.signals = ['closed', 'beep', 'leds', 'title']

        self.keygrab = False
        self.last_key = None

        self.response_buffer = []

        self.term_modes = TermModes()

        self.width = None
        self.height = None
        self.term = None
        self.has_focus = False
        self.terminated = False
        self._feed_data = ''

    def beep(self):
        """Emit beep signal."""
        self._emit('beep')

    def leds(self, which):
        """
        Emit leds signal.

        Parameters
        ----------
        which: list
            Leds to turn on.
        """
        self._emit('leds', which)

    def touch_term(self, width, height):
        """
        Update terminal size.

        Parameters
        ----------
        width: int
            Terminal width.
        height: int
            Terminal height.
        """
        if self.width == width and self.height == height:
            return

        if not self.term:
            self.term = TermCanvas(width, height, self)
        else:
            self.term.resize(width, height)

        self.width = width
        self.height = height

    def set_title(self, title):
        """
        Set terminal title.

        Parameters
        ----------
        title : str
            Terminal title.
        """
        self._emit('title', title)

    def change_focus(self, has_focus):
        """Ignore SIGINT if this widget has focus."""
        if self.terminated or self.has_focus == has_focus:
            return
        self.has_focus = has_focus
        if has_focus:
            self.old_tios = RealTerminal().tty_signal_keys()
            RealTerminal().tty_signal_keys(*(['undefined'] * 5))
        else:
            RealTerminal().tty_signal_keys(*self.old_tios)

    def render(self, size, focus=False):
        """
        Render widget.

        Parameters
        ----------
        size : tuple
            Width, height tuple.
        focus : bool, optional
            Current focus state.
        """
        if not self.terminated:
            self.change_focus(focus)

            width, height = size
            self.touch_term(width, height)

        return self.term

    def clear(self):
        """Clear the terminal."""
        if not self.term:
            return
        self.term.clear()

    def selectable(self):
        """Terminal isn't selectable. Return False."""
        return False

    def feed(self, data):
        """
        Add new line to the terminal.

        Parameters
        ----------
        data : str
            Line to add
        """
        if self.term:
            if isinstance(data, str):
                data = data.encode('utf-8', 'ignore')
            self.term.addstr(data)

    def keypress(self, size, key):
        """
        Handle keypress event.

        Parameters
        ----------
        size : tuple
            Terminal size (width, height).
        key : str
            Pressed key.
        """
        if self.terminated:
            return key

        if key == 'window resize':
            width, height = size
            self.touch_term(width, height)
            return

        if key == 'page up':
            self.term.scroll_buffer()
            self._invalidate()
            return
        elif key == 'page down':
            self.term.scroll_buffer(up=False)
            self._invalidate()
            return
        else:
            # hand down keypress
            return key
