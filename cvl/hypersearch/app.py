# -*- coding: utf-8 -*-

"""Hyper-params search application with Urwid-based UI."""


import os
import errno
import subprocess
import pty
import urwid
import atexit
import sys
import csv
import collections as coll

from functools import reduce

from cvl.utils import timer
from .stacked_dterm import StackedDTerm
from .common import get_all_keys, get_flatten_mapping,\
    restore_mapping_hierarchy

ProcessInfo = coll.namedtuple('ProcessInfo', ['process', 'fd', 'wd'])
FormatArg = coll.namedtuple('FormatArg', ['help', 'fn'])


class ExperimentRun(object):
    """
    Information about single experiment run.

    Attributes
    ----------
    objective : float
        Objective function value.
    sample_point : SamplePoint
        Some point in hyper-params space.
    sspace_objective : float
        Objective function value transformed to the search space.
    sspace_point : SamplePoint
        Search space point. Corresponds to ``sample_point`` point from
        hyper-parameters space.
    sspace_variance : float
        Variance transformed to search space
    step : int
        Optimisation step (sequential).
    time : float
        Time of experiment run (in seconds).
    variance : float
        Sampled objective function variance.
    """

    def __init__(self, sample_point, sspace_point, objective=None,
                 variance=None, sspace_objective=None, sspace_variance=None,
                 time=None, step=None):
        """
        Create expriment instance.

        Parameters
        ----------
        sample_point : SamplePoint
            Some point from hyper-params space.
        sspace_point : SamplePoint
            Correspond to ``sample_point`` point from search space.
        objective : float, optional
            Objective function value.
        variance : float, optional
            Sampled objective function variance.
        sspace_objective : float, optional
            Objective transformed to search space.
        sspace_variance : float, optional
            Variance transformed to the search space.
        time : float, optional
            Time of experiment run (in seconds).
        step : int, optional
            Optimisation step (consecutive numbers).
        """
        self.sample_point = sample_point
        self.sspace_point = sspace_point
        self.objective = objective
        self.variance = variance
        self.sspace_objective = sspace_objective
        self.sspace_variance = sspace_variance
        self.time = time
        self.step = step


class HyperSearchApp(object):
    """
    Hyper-params search application with urwid-based interface.

    Constructor starts event loop that handles all application logic.

    Attributes
    ----------
    FORMAT_SUBSTITUTIONS : dict
        Mapping of available substitutions for CmdLineBuilder.
    run_data_samples : list
        Experiment samples.
    """

    #: Mapping of available substitutions for CmdLineBuilder.
    FORMAT_SUBSTITUTIONS = {
        # dummy entry to document default CmdLineBuilder substitution
        'name': FormatArg(
            'unique configuration name (made of parameter names and values)',
            None  # None means not to call and not to pass
        ),
        'worker': FormatArg(
            'worker identifier (zero-based)',
            lambda self=None, **kwargs: kwargs.get('wid', 0)
        ),
        'num_workers': FormatArg(
            'worker identifier (zero-based)',
            lambda self=None, **kwargs: getattr(self, '_num_workers', 1)
        ),
        'total_steps': FormatArg(
            'total number of steps',
            lambda self=None, **kwargs: getattr(self, '_total_steps', 1)
        ),
        'steps_done': FormatArg(
            'number of completed steps',
            lambda self=None, **kwargs: getattr(self, '_steps_done', 0)
        )
    }

    _steps_done = 0
    _last_started_step = 0

    def __init__(self, num_workers, total_steps, hparams, verbose, objective,
                 cmdline_builder, backend, logfile=None, env=None,
                 hist_file=None):
        """
        Create hyper-params search application with UI based on urwid.

        Start event loop that handles all application logic.

        Parameters
        ----------
        num_workers : int
            Number of parallel workers.
        total_steps : int
            Total number of optimization steps.
        hparams : HyperParams
            Hyper-parameters collection.
        verbose : int
            Application verbosity level.
        objective : Objective
            Objective function definition corresponding to the structure.
        cmdline_builder : CmdLineBuilder
            Run command line builder instance.
        backend : AbstractBackend
            Optimization backend instance.
        logfile : file, optional
            Log file descriptor.
        env : dict, optional
            Environment variables for spawned processes.
            May use format substitutions.
        hist_file : str, optional
            Path to a history file. If not empty will load experiment history
            from the file and continue search. All experiment samples are
            saved to this file.
        """
        self._num_workers = num_workers
        self._total_steps = total_steps
        self._verbose = verbose
        self._objective = objective
        self._cmdline_builder = cmdline_builder
        self._backend = backend
        self._hparams = hparams
        self._logfile = logfile
        self._env = env or []
        self._keyseq = []

        self.run_data_samples = []
        self._best_sample_index = 0
        self._hist_file = hist_file

        urwid.set_encoding('utf8')

        self._stacked_term = StackedDTerm()
        self._status_firstline_widget = urwid.Text('Timer')
        self._status_nextline_widget = urwid.Text('Left pane')
        self._leftpane = urwid.Pile([
            self._status_firstline_widget,
            self._status_nextline_widget
        ])
        self._rightpane = urwid.GridFlow(
            [urwid.Text(hparam.name) for hparam in self._hparams.values()],
            10, 3, 0, 'left'
        )
        self._mainbox = urwid.LineBox(
            self._stacked_term,
            title='Log',
            tlcorner=u'─',
            tline=u'─',
            lline=u' ',
            trcorner=u'─',
            blcorner=u' ',
            rline=u' ',
            bline=u' ',
            brcorner=u' '
        )
        self._mainframe = urwid.Frame(
            self._mainbox,
            urwid.LineBox(urwid.Columns(
                [
                    self._leftpane,
                    ('fixed', 3, urwid.Padding(urwid.SolidFill(u'│'),
                                               left=1, right=1)),
                    self._rightpane
                ],
                box_columns=[1]
            )),
        )
        self._loop = urwid.MainLoop(
            self._mainframe,
            handle_mouse=False,
            unhandled_input=self._handle_key
        )

        # create terminal for log
        self._stacked_term.add_term()

        if hist_file is not None:
            hist_file = open(hist_file, 'a+')
            # create dummy experiment to get correct keys for saving
            exp = ExperimentRun(hparams.start_sample(), hparams.start_sample())
            csv_keys = get_all_keys(vars(exp))
            # read history
            reader = csv.DictReader(hist_file)
            if reader.fieldnames is not None:
                if set(csv_keys) != set(reader.fieldnames):
                    sys.exit(
                        'Non-empy history is not compatible with current '
                        'setup: {0} vs {1}'.format(csv_keys, reader.fieldnames)
                    )
                csv_keys = reader.fieldnames

            for record in reader:
                exp = ExperimentRun(None, None)
                for name, value in restore_mapping_hierarchy(record).items():
                    setattr(exp, name, value)
                self.run_data_samples.append(exp)
            del reader

            self._hist_file = hist_file
            self._steps_done = len(self.run_data_samples)
            self._last_started_step = len(self.run_data_samples)
            # find best index
            for i in range(self._steps_done):
                if (self.run_data_samples[i].sspace_objective <
                    self.run_data_samples[
                        self._best_sample_index].sspace_objective):
                    self._best_sample_index = i
                # populate backend with data from loaded history
                self._backend.add_observation(
                    self.run_data_samples[i].sspace_point,
                    self.run_data_samples[i].sspace_objective,
                    self.run_data_samples[i].sspace_variance
                )
            self._hist_writer = csv.DictWriter(self._hist_file, csv_keys)
            if self._steps_done == 0:
                self._hist_writer.writeheader()
            else:
                print('Loaded history records: {}'.format(self._steps_done))

        if self._steps_done == 0:
            # initial sample points
            start_sample = hparams.start_sample()
            self._current_runs = [ExperimentRun(
                start_sample, self._hparams.transform(start_sample, False))]
            for i in range(self._num_workers - 1):
                sample = hparams.random_sample()
                self._current_runs.append(ExperimentRun(
                    sample, self._hparams.transform(sample, False)))
        else:
            # ask backend for next points (vs random?)
            self._current_runs = []
            for i in range(self._num_workers):
                next_sample_sspace = self._backend.get_suggestions()[0]
                next_sample = self._hparams.transform(
                    next_sample_sspace, True)
                self._current_runs.append(ExperimentRun(
                    next_sample, next_sample_sspace))

        self._workers = [None] * self._num_workers
        self._workers_output = [''] * self._num_workers

        atexit.register(self._cleanup)
        timer.start('hyper-search')

        # spawn initial workers and create workers terminals
        for i in range(self._num_workers):
            self._stacked_term.add_term()

        self._update_ui()
        self._update_run_time(None, None)

    def run(self):
        """Start event loop and do all processing until finish or cancel."""
        self._loop.set_alarm_in(0.1, self._startup)
        self._loop.run()

    def _handle_key(self, key):
        """
        Handle keypress events.

        Parameters
        ----------
        key : str
            Pressed key

        Raises
        ------
        urwid.ExitMainLoop
            Raised to signal urwid about application exit.
        """
        if key in {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'}:
            self._keyseq.append(int(key))
            index = reduce(lambda n, x: n * 10 + x, self._keyseq)
            if index * 10 > self._num_workers:
                self._keyseq = []
                self._stacked_term.show_term(index)
                self._update_ui()
                return

            self._loop.set_alarm_in(0.5, self._handle_keyseq)
        elif len(self._keyseq) > 0:
            index = reduce(lambda n, x: n * 10 + x, self._keyseq)
            self._keyseq = []
            self._stacked_term.show_term(index)
            self._update_ui()
            return

        if key == 'left':
            self._stacked_term.show_previous_term()
            self._update_ui()
        elif key == 'right':
            self._stacked_term.show_next_term()
            self._update_ui()
        elif key in ('q', 'Q'):
            if timer.elapsed('hyper-search-quit', True) < 1:
                raise urwid.ExitMainLoop()
            self._stacked_term.show_term(0)
            self._log('\r\n\r\nPress Q again within 1 second to stop '
                      'optimization and exit\r\n\r\n')
            timer.start('hyper-search-quit')

    def _spawn_worker(self, wid):
        """
        Spawn worker process for specified worker ID (wid).

        Parameters
        ----------
        wid : int
            Worker id.
        """
        sample_point = self._current_runs[wid].sample_point
        sspace_point = self._current_runs[wid].sspace_point
        self._last_started_step += 1
        self._current_runs[wid].step = self._last_started_step
        format_values = {
            name: value.fn(self, wid=wid) for name, value
            in HyperSearchApp.FORMAT_SUBSTITUTIONS.items()
            if value.fn is not None
        }
        cmdline = self._cmdline_builder.build(sample_point, format_values)
        env = {varname: varformat.format(**format_values)
               for varname, varformat in self._env}
        proc_env = dict(os.environ)
        proc_env.update(env)

        self._log(
            '\r\n[{wid}] Starting step {0} of {1}\r\n'.format(
                self._last_started_step, self._total_steps, wid=wid)
        )
        self._log('[{wid}] Hyper params:\r\n'.format(wid=wid), 1)
        for name, value in sample_point.items():
            hparam = self._hparams[name]
            self._log(
                (
                    '[{wid}]\t{hp.name} ∈ [{hp.min}, {hp.max}] → {value} '
                    '({value_opt})\r\n'
                ).format(value=value,
                         wid=wid,
                         value_opt=sspace_point[name],
                         hp=hparam),
                1
            )
        self._log('[{wid}] Environment: {0}\r\n'.format(
            '\r\n'.join(['{0}={1}'.format(name, value)
                         for name, value in env.items()]),
            wid=wid), 1)
        self._log('[{wid}] Executing {0}\r\n'.format(' '.join(cmdline),
                                                     wid=wid), 1)
        master_fd, slave_fd = pty.openpty()

        self._stacked_term.clear(wid + 1)
        self._workers_output[wid] = ''
        timer.start(wid)
        try:
            process = subprocess.Popen(
                cmdline,
                stdout=slave_fd, stderr=subprocess.STDOUT,
                close_fds=True, env=proc_env
            )
        except Exception as e:
            msg = 'Execution failed:\n{0}\n{1}'.format(' '.join(cmdline), e)
            self._log(msg)
            sys.exit(msg)
        setattr(process, 'cmdline', cmdline)
        os.close(slave_fd)
        watch_descriptor = self._loop.watch_file(
            master_fd,
            lambda wid=wid: self._handle_worker_output(wid)
        )
        self._workers[wid] = ProcessInfo(process, master_fd, watch_descriptor)
        self._update_ui()

    def _handle_worker_output(self, wid):
        """
        Process worker output and act in a corresponding way.

        Parameters
        ----------
        wid : int
            Worker ID

        Raises
        ------
        urwid.ExitMainLoop
            Signal urwid to exit event loop.
        """
        line = ''
        try:
            line = os.read(self._workers[wid].fd, 512)
            if line:
                self._stacked_term.feed(line, wid + 1)
                line = line.decode('utf-8', 'ignore')
                lines = (self._workers_output[wid] + line).splitlines(True)
                self._workers_output[wid] = lines[-1]
                return
        except OSError as e:
            if e.errno != errno.EIO:
                raise
        # process finished: cleanup
        os.close(self._workers[wid].fd)
        self._workers[wid].process.wait()
        elapsed_time = timer.stop(wid, return_only=True)
        self._loop.remove_watch_file(self._workers[wid].wd)
        self._workers[wid] = None

        # increase finished counter, obtain sampled values
        self._steps_done += 1
        line = self._workers_output[wid]
        values = self._objective.parse(line, elapsed_time)

        if values is None:
            self._log(
                ('[{wid}] Failed to find objective value in command output\r\n'
                 '[{wid}] Last output line was:\r\n'
                 '[{wid}] {0}\r\n\n').format(line, wid=wid))
            return

        objective, variance, sspace_objective, sspace_variance = values

        self._current_runs[wid].objective = objective
        self._current_runs[wid].sspace_objective = sspace_objective
        self._current_runs[wid].variance = variance
        self._current_runs[wid].sspace_variance = sspace_variance
        self._current_runs[wid].time = elapsed_time
        self._log(
            '\r\n[{wid}] Last output: {line}\r\n'.format(
                wid=wid, run=self._current_runs[wid], line=line),
            verbosity=1
        )
        self._log(
            ('\r\n[{wid}] Sampled objective and variance for step {run.step}: '
             '{run.objective} ({run.sspace_objective}) {run.variance} '
             '({run.sspace_variance})\r\n\r\n').format(
                wid=wid, run=self._current_runs[wid])
        )
        self.run_data_samples.append(self._current_runs[wid])
        # add history record
        if self._hist_file is not None:
            self._hist_writer.writerow(
                get_flatten_mapping(vars(self._current_runs[wid])))
            self._hist_file.flush()
            os.fsync(self._hist_file.fileno())

        # update best index
        if (self.run_data_samples[self._best_sample_index].sspace_objective >
                sspace_objective):
            self._best_sample_index = len(self.run_data_samples) - 1

        # check if finished
        if self._steps_done == self._total_steps:
            self._current_runs[wid] = None
            self._log('\r\nOptimization finished\r\n')
            raise urwid.ExitMainLoop()

        # check if next worker has something to do
        if self._last_started_step >= self._total_steps:
            self._current_runs[wid] = None
            return

        # obtain next sample point
        self._backend.add_observation(
            self._current_runs[wid].sspace_point,
            self._current_runs[wid].sspace_objective,
            self._current_runs[wid].sspace_variance
        )
        next_sample_sspace = self._backend.get_suggestions()[0]
        next_sample = self._hparams.transform(
            next_sample_sspace, True)
        self._current_runs[wid] = ExperimentRun(next_sample,
                                                next_sample_sspace)
        self._log('[{wid}] Next point to sample:\r\n'.format(wid=wid), 1)
        for name, value in next_sample.items():
            self._log('[{wid}] \t{0}: {1} ({2})\r\n'.format(
                self._hparams[name].name, value, next_sample_sspace[name],
                wid=wid), 1)
        self._log('\r\n', 1)

        # start next worker
        self._spawn_worker(wid)

    def _startup(self, loop, udata):
        """
        Perform initial startup actions.

        Parameters
        ----------
        loop : MainLoop
            Urwid event loop
        udata : None
            User data (not used)
        """
        self._backend.logger = self._log
        for i in range(self._num_workers):
            self._spawn_worker(i)

    def _log(self, msg, verbosity=0):
        """
        Log message with passed verbosity.

        Parameters
        ----------
        msg : str
            Message
        verbosity : int, optional
            Message verbosity level
        """
        if self._verbose < verbosity:
            return
        self._stacked_term.feed(msg, 0)
        if self._logfile is not None:
            self._logfile.write(msg)

    def _cleanup(self):
        """Run cleanup actions upon exit."""
        for worker in self._workers:
            if worker is None:
                continue
            worker.process.terminate()

    def _update_ui(self):
        """Update user interface."""
        current_index = self._stacked_term.index
        if current_index == 0:
            # log tab
            self._mainbox.set_title('Log')
            maxlen = 0
            if len(self.run_data_samples) > 0:
                best_point = self.run_data_samples[self._best_sample_index]
            else:
                best_point = self._current_runs[0]
            for name, (w, o) in zip(self._hparams.keys(),
                                    self._rightpane.contents):
                hparam = self._hparams[name]
                text = u'{strip_name} ∈ [{hp.min}, {hp.max}] ⇾ {best}'.format(
                    strip_name=hparam.name.strip('- '),
                    hp=hparam,
                    best=best_point.sample_point[name]
                )
                maxlen = max(maxlen, len(text))
                w.set_text(text)
            self._rightpane.cell_width = maxlen
            self._status_nextline_widget.set_text(
                u'Workers: {workers} (use ← / ⇾ or digits to navigate)\n'
                'Steps: {done} / {total}\n'
                'Best objective: {objective} ({sspace_objective}) '
                '@ step {bstep}'.format(
                    total=self._total_steps,
                    done=self._steps_done,
                    workers=self._num_workers,
                    objective=best_point.objective or u'?',
                    sspace_objective=best_point.sspace_objective or u'?',
                    bstep=best_point.step
                )
            )
        else:
            # other tabs
            self._mainbox.set_title('Worker {0}'.format(current_index - 1))
            sample_point = self._current_runs[current_index - 1].sample_point
            sspace_point = self._current_runs[current_index - 1].sspace_point
            maxlen = 0
            # TODO: support fo structured search space
            for name, hparam, (w, o) in zip(self._hparams.keys(),
                                            self._hparams.values(),
                                            self._rightpane.contents):
                text = u'{name} = {value} ({value_opt})'.format(
                    name=hparam.name.strip('- '),
                    value=sample_point.get(name, '∅'),
                    value_opt=sspace_point.get(name, '∅')
                )
                maxlen = max(maxlen, len(text))
                w.set_text(text)
            self._rightpane.cell_width = maxlen
            if self._workers[current_index - 1] is not None:
                self._status_nextline_widget.set_text(
                    'Command line:\n{cmdline}'.format(
                        cmdline=' '.join(
                            self._workers[current_index - 1].process.cmdline)
                    )
                )

    def _update_run_time(self, loop, udata):
        """
        Update running timers.

        Parameters
        ----------
        loop : EventLoop
            Urwid event loop.
        udata : Mixed
            User data (not used)
        """
        time = timer.format_time(timer.elapsed('hyper-search', True), 0)
        current_index = self._stacked_term.index
        if current_index == 0:
            self._status_firstline_widget.set_text(
                'Optimization run time: {time}'.format(time=time))
        else:
            etime = timer.format_time(
                timer.elapsed(current_index - 1, True), 0)
            self._status_firstline_widget.set_text(
                'Optimization run time: {time} ({etime})'.format(time=time,
                                                                 etime=etime))
        self._loop.set_alarm_in(1, self._update_run_time)

    def _handle_keyseq(self, loop, udata):
        """
        Send dummy keypress event to handle key sequence.

        Method called as a timeout callback to handle end of sequence

        Parameters
        ----------
        loop : EventLoop
            Urwid event loop.
        udata : Mixed
            User data (not used)
        """
        # send dummy key to trigger keysequence action
        self._handle_key(None)
