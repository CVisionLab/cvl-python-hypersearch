# -*- coding: utf-8 -*-

"""Misc usefull functions and classes implementations."""

import collections as coll
import numpy as np

from sympy import Symbol
from sympy.parsing.sympy_parser import parse_expr


class Transform(object):
    """
    Data transform definition.

    Transform doesn't specify number of input arguments.

    Attributes
    ----------
    description : TYPE
        Transform description.
    inp : callable
        Direct transform.
    out : callable
        Inverse transform.
    """

    def __init__(self, inp=None, out=None, description=''):
        """
        Create transform instance.

        Parameters
        ----------
        inp: callable or str, optional
            Direct data transform. If str then will be parsed by sympy.
            Identity by default.
        out: callable or str, optional
            Inverse data transform. If str then will be parsed by sympy.
            Identity by default.
        description: str, optional
            Transform description
        """
        self.inp = inp or (lambda **kwargs: next(iter(kwargs.values())))
        if not isinstance(self.inp, coll.Callable):
            self.inp = sympy2fn(self.inp)
        self.out = out or (lambda **kwargs: next(iter(kwargs.values())))
        if not isinstance(self.out, coll.Callable):
            self.out = sympy2fn(self.out)
        self.description = description


class Objective(object):
    """
    Objective definition. Stores regex, transforms and extra info.

    Objective function information and transforms.

    Attributes
    ----------
    assume_inf : bool
        Assume objective is infinite if not found
    objective_transform : `Transform`
        Objective transform
    regexp : re.Pattern
        Regular expression that captures objective value and variance
    variance_transform : `Transform`
        Variance transform
    """

    def __init__(self, regexp, objective_transform=None,
                 variance_transform=None, assume_inf=False):
        """
        Construct Objective object.

        Parameters
        ----------
        regexp : re.Pattern
            Regular expression to capture objective value.
        objective_transform : `Transform` or str, optional
            Objective value transform.
            Can be just forward transform in sympy format.
        variance_transform : `Transform` or str, optional
            Objective variance transform.
            Can be just forward transform in sympy format.
        assume_inf : bool, optional
            Assume objective is infinite if not found (in output).
        """
        self.regexp = regexp
        if not isinstance(objective_transform, Transform):
            self.objective_transform = Transform(objective_transform)
        else:
            self.objective_transform = objective_transform
        if not isinstance(variance_transform, Transform):
            self.variance_transform = Transform(variance_transform)
        else:
            self.variance_transform = variance_transform
        self.assume_inf = assume_inf

    def parse(self, line, time):
        """
        Find objective and variance in passed string.

        Returns None if objective was not found in the passed string

        Parameters
        ----------
        line : str
            String to parse
        time : float
            Elapsed time (will be passed to objective transform)

        Returns
        -------
        tuple
            (objective, variance, sspace_objective, sspace_variance)
            tuple or None if objective not found and assume_inf is False.
        """
        match = self.regexp.search(line)
        finfo = np.finfo(np.float32)
        if match is None:
            if not self.assume_inf:
                return None
            objective = finfo.max
            variance = finfo.max
        else:
            objective = float(match.group('value'))
            variance = float(match.group('variance') or objective)
        sspace_objective = self.objective_transform.inp(x=objective, t=time)
        sspace_variance = self.variance_transform.inp(x=variance)
        return (np.clip(objective, finfo.min, finfo.max),
                np.clip(variance, finfo.min, finfo.max),
                np.clip(sspace_objective, finfo.min, finfo.max),
                np.clip(sspace_variance, finfo.min, finfo.max))


class SamplePoint(coll.OrderedDict):
    """
    A point in search space.

    It may be returned as a result of backend's call get_suggestions()
    and in this case backend may setup suggestion ↔ observation correspondence
    using suggestion attribute

    Attributes
    ----------
    suggestion : object
        Backends may store whatever they want in this
        attribute if they need to maintain observation / suggestion
        correspondence.
    """

    suggestion = None


def sympy2fn(expr):
    """
    Convert sympy expression to callable python function.

    Returned function accepts keyword arguments with names matching
    free variables found in the expr.

    Parameters
    ----------
    expr: str
        Sympy expression as a string.

    Returns
    -------
    callable
        A function that can be called to evaluate symbolic expression.
    """
    expr = parse_expr(expr)
    return lambda **kwargs: float(expr.evalf(
        subs={Symbol(name): value for name, value in kwargs.items()})
    )


def get_all_vals(mapping):
    """
    Return list of all values found in the mapping including inner mappings.

    Parameters
    ----------
    mapping: mapping
        Some mapping like dictionary object.

    Returns
    -------
    list
        List of values found in the mapping and all inner mappings.
    """
    vals = []
    for name in sorted(mapping.keys()):
        if isinstance(mapping[name], dict):
            vals.extend(get_all_vals(mapping[name]))
        else:
            vals.append(mapping[name])
    return vals


def get_all_keys(mapping):
    """
    Return list of all keys found in mapping including keys of inner mappings.

    Keys of inner mappings are prefixed with "<parent-key>/".

    Parameters
    ----------
    mapping: mapping
        Some mapping like dictionary object.

    Returns
    -------
    list
        List of keys found in the mapping and all inner mappings.
    """
    keys = []
    for name in sorted(mapping.keys()):
        if isinstance(mapping[name], dict):
            for key in get_all_keys(mapping[name]):
                keys.append('{0}/{1}'.format(name, key))
        else:
            keys.append(name)
    return keys


def get_flatten_mapping(mapping):
    """
    Get flatten mapping from the passed mapping.

    If there is ``mapping[key1] = inner_mapping`` then in the result it will
    be: ``mapping[key1/inner_mapping.key1] = inner_mapping[key1]``.

    Parameters
    ----------
    mapping: mapping
        Some mapping like dictionary object.

    Returns
    -------
    dict
        New mapping object without inner mappings (flatten).
    """
    result = {}
    for name in sorted(mapping.keys()):
        if isinstance(mapping[name], dict):
            for key, value in get_flatten_mapping(mapping[name]).items():
                result['{0}/{1}'.format(name, key)] = value
        else:
            result[name] = mapping[name]
    return result


def restore_mapping_hierarchy(mapping):
    """
    Restore mapping hierarchy that was flattened.

    Function assumes that hierarchy embedded into key names using "/" symbol.
    Also tries to restore numerical types.

    Parameters
    ----------
    mapping: mapping
        A mapping flattened with `get_flatten_mapping`.

    Returns
    -------
    dict
        Mapping with restored hierarchy.
    """
    result = {}
    for key in mapping:
        path = key.split('/')
        if len(path) > 1:
            current = result
            for name in path[:-1]:
                if name not in current:
                    current[name] = {}
                current = current[name]
            value = float(mapping[key])
            if value.is_integer():
                value = int(value)
            current[path[-1]] = value
        else:
            value = float(mapping[key])
            if value.is_integer():
                value = int(value)
            result[key] = value
    return result
