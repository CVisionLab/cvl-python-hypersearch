# -*- coding: utf-8 -*-

"""Command-line run line builder."""

import re


class CmdLineBuilder(object):
    """Run command-line builder from hyper-params, fixed and format params."""

    def __init__(self, hparams, fixed_args, format_args=None, order=None):
        """
        Create builder from params.

        Parameters
        ----------
        hparams: HyperParams
            Hyper-parameters class object.
        fixed_args: list
            Fixed(positional) arguments.
            The first item treated as program name.
        format_args: list, optional
            Format parameters, default is ``[]``.
        order: list, optional
            Arguments order. Default is ``['binary', 'format', 'fixed',
            'hparams']``.

        Raises
        ------
        RuntimeError
            Unexpected value in order argument.
        """
        if format_args is None:
            format_args = []
        if order is None:
            order = ['binary', 'format', 'fixed', 'hparams']
        self._binary = fixed_args[:1]
        self._fixed_args = fixed_args[1:]
        self._format_args = format_args or []
        self._hparams = hparams
        if not {'binary', 'format', 'fixed', 'hparams'}.issuperset(order):
            raise RuntimeError(
                'Unrecognized value in order param: {0}'.format(order))
        self._order = order

    def build(self, sample_point, format_values=None):
        """
        Build run command-line.

        Parameters
        ----------
        sample_point: dict
            Sampling point.
        format_values: dict, optional
            Format values for format args.

        Returns
        -------
            list
                Run command line as list of parts.

        """
        cmdline_hparams = []
        for name in self._hparams:
            cmdline_hparams.append(self._hparams[name].name)
            cmdline_hparams.append(str(sample_point[name]))

        cmdline_format = []
        format_values = format_values or {}
        if 'name' not in format_values:
            name = '-'.join(cmdline_hparams)
            name = re.sub('-{2,}', '-', name).strip('-')
            format_values['name'] = name

        for argname, argformat in self._format_args:
            cmdline_format += [argname.lstrip(),
                               argformat.format(**format_values)]

        result = {
            'binary': self._binary,
            'format': cmdline_format,
            'fixed': self._fixed_args,
            'hparams': cmdline_hparams
        }

        return sum([result[item_name] for item_name in self._order], [])
