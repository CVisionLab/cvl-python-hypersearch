#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Simple application for testing hyper-search tool."""

import time
import argparse

from sympy import Symbol
from sympy.parsing.sympy_parser import parse_expr
import numpy as np

from cvl.utils import console

from .version import __version__


def main():
    """
    Application entry point.
    """
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    parser.add_argument(
        '--version', '-V', action='version',
        version=f'%(prog)s {__version__}'
    )
    parser.add_argument('--x', type=float, default=0.0,
                        help='value of variable x')
    parser.add_argument('--y', type=float, default=0.0,
                        help='value of variable y')
    parser.add_argument('--z', type=float, default=0.0,
                        help='value of variable z')
    parser.add_argument('--variance', '-v', type=float, default=0.0,
                        help='variance value')
    parser.add_argument('--randomize', '-r', action='store_true',
                        help='Add random noise to the result')
    parser.add_argument('--num-steps', '-s', type=int, default=1,
                        help='number of steps to do (emulates long running '
                        'task)')
    parser.add_argument('--num-iters', '-i', type=int, default=100,
                        help='number of iterations in each step (each '
                        'iteration has delay of 0.1s)')
    parser.add_argument('--func', '-f', default='(x - 1) ** 2 + (y - 1) ** 2',
                        help='sympy expression that defines functions to '
                        'optimize')
    args = parser.parse_args()

    expr = parse_expr(args.func)

    for j in range(args.num_steps):
        print('Starting next step')
        print()
        for i in range(args.num_iters):
            console.progress(i + 1, args.num_iters, 'running', eta=True)
            time.sleep(0.1)

        print('Done')
    value = float(
        expr.evalf(
            subs={
                Symbol('x'): args.x,
                Symbol('y'): args.y,
                Symbol('z'): args.z
            }
        )
    )
    if args.randomize:
        if not args.variance:
            args.variance = 0.001
        value += np.random.normal(0, args.variance)
    print(value, args.variance)


if __name__ == '__main__':
    main()
