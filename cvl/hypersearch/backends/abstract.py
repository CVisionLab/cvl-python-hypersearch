# -*- coding: utf-8 -*-

"""Abstract optimization backend."""

import abc
import collections


class AbstractBackend(object):
    """
    Abstract base class for implementing optimization backends.
    """

    __metaclass__ = abc.ABCMeta
    _logger = None
    _logged_messages = None

    def __init__(self):  # noqa
        self._logged_messages = []

    @abc.abstractmethod
    def get_suggestions(self, num_points=1):
        """
        Get the next suggestions (points to sample).

        Abstract method. Must be overwritten in subclasses to return list
        of the points to sample next.

        Parameters
        ----------
        num_points : int, optional
            Number of suggestions to return.

        Returns
        -------
        list
            List of suggestions. Each suggestion is a dict.
        """
        pass

    @abc.abstractmethod
    def add_observation(self, sample_point, objective, variance):
        """
        Add observation point.

        Observations are points in search space with known objective and
        possibly variance.

        Abstract method, must be overwritten in subclasses.

        Parameters
        ----------
        sample_point : dict
            Sample point (the point observation was sampled at).
        objective : float
            Observed objective value.
        variance : float
            Observed objective variance.
        """
        pass

    @staticmethod
    @abc.abstractmethod
    def populate_parser(argument_group):
        """
        Populate argument parser with backend arguments.

        Return argument parser that should be added to application's
        parser. Subclasses should return parser with arguments specific to
        backend.

        Abstract method, must be overwritten in subclasses.

        Parameters
        ----------
        argument_group : argparse.ArgumentGroup
            Argument parser group to
            populate with backend options

        """
        pass

    def log(self, msg, level):
        """
        Log message with the specified verbosity level.

        Backend's user controls whether logging enabled or not.

        Parameters
        ----------
        msg : str
            Message to log.
        level : int
            Message log level (it is up to backend user how to use
            this information).
        """
        if self._logger is not None:
            self._logger(msg, level)
        elif self._logged_messages is not None:
            self._logged_messages.append((msg, level))

    @property
    def logger(self):
        """
        Get logger instance.

        The instance must be set by implementation to enable logging.
        Until the logger is set, all logged messages are collected in the
        internal list and sent to the ``logger`` once it set.

        Returns
        -------
        callable
            Current logger.
        """
        return self._logger

    @logger.setter
    def logger(self, value):
        """Set logger instance."""
        if not isinstance(value, collections.Callable):
            raise TypeError('Logger must be callable')
        self._logger = value
        if self._logged_messages is not None:
            for args in self._logged_messages:
                self._logger(*args)
            self._logged_messages = None

    @staticmethod
    @abc.abstractmethod
    def enabled():
        """
        Return if backend is enabled.

        Returns True if the backend is enabled or string with description of
        the reason the backend can't be enabled (e.g. missing dependencies).

        Abstract method, must be implemented in subclasses.
        """
        pass
