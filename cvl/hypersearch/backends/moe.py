# -*- coding: utf-8 -*-

"""MOE optimization backend."""

import collections as coll

from .abstract import AbstractBackend
from ..common import SamplePoint
try:
    import moe.easy_interface.experiment as experiment
    import moe.easy_interface.simple_endpoint as simple_endpoint
    import moe.optimal_learning.python.data_containers as data_containers
except ImportError:
    _enabled = False
else:
    _enabled = True


class MOEBackend(AbstractBackend):
    """MOE-based hyper-search backend."""

    def __init__(self, search_space, args):
        """
        Create ``MOEBackend`` instance.

        Parameters
        ----------
        search_space : dict
            Search space specification:
            ``{'param1': [min, max], 'param2': [min, max]}``.
        args : argparse.Namespace
            Command line parameters.

        Raises
        ------
        RuntimeError
            Bad number of MOE hyper-parameters.
        """
        super(MOEBackend, self).__init__()
        self._search_space = search_space
        self._experiment = experiment.Experiment([bounds for bounds in
                                                  search_space.values()])
        self._host = args.moe_host
        self._port = args.moe_port
        if (args.moe_hyperparameters is not None and
                len(args.moe_hyperparameters) != len(self._search_space) + 1):
            raise RuntimeError('Number of MOE hyperparameters must be (D+1) '
                               '= {0}'.format(len(self._search_space) + 1))

        self._options = self._setup_options(
            {
                'covariance_info': {
                    'covariance_type': str,
                    'hyperparameters': lambda s: map(float, s)
                },
                'optimizer_info': {
                    'optimizer_type': str,
                    'num_multistarts': int,
                    'num_random_samples': int,
                    'optimizer_parameters': {
                        'max_num_steps': int
                    }
                }
            },
            {
                'covariance_info': {
                    'covariance_type': 'square_exponential',
                },
                'optimizer_info': {
                    'optimizer_type': 'gradient_descent_optimizer',
                    'num_multistarts': 50,
                    'num_random_samples': 400,
                    'optimizer_parameters': {
                        'max_num_steps': 300
                    }
                }
            },
            args
        )

    def get_suggestions(self, num_points=1):
        """
        Get next suggestions.

        Parameters
        ----------
        num_points : int, optional
            Number of suggestions to return

        Returns
        -------
        list
            List of suggestions. Each suggestion is a dict
        """
        return [
            SamplePoint({name: value for name, value in
                         zip(self._search_space.keys(), sample)})
            for sample in simple_endpoint.gp_next_points(
                self._experiment,
                rest_host=self._host,
                rest_port=self._port,
                num_to_sample=num_points,
                **self._options
            )
        ]

    def add_observation(self, sample_point, objective, variance):
        """
        Add observation point.

        Parameters
        ----------
        sample_point : dict
            Sample point (the point observation was sampled at)
        objective : float
            Observed objective value.
        variance : float
            Observed objective variance.
        """
        self._experiment.historical_data.append_sample_points(
            [data_containers.SamplePoint(
                [sample_point[name] for name in self._search_space.keys()],
                objective,
                variance
            )]
        )

    def _setup_options(self, optgroups, defaults, args):
        """
        Set backend options.

        Parameters
        ----------
        optgroups : dict
            Options definitions
        defaults : dics
            Options defaults
        args : argparse.Namespace
            Parse command line arguments.

        Returns
        -------
        dict
            Backend options mapping.
        """
        for itemname, conversion in optgroups.items():
            argname = 'moe_' + itemname
            if getattr(args, argname, None) is None:
                continue
            if isinstance(conversion, coll.Callable):
                defaults[itemname] = conversion(getattr(args, argname))
            elif isinstance(conversion, coll.Mapping):
                defaults[itemname] = self._setup_options(
                    optgroups[itemname],
                    defaults.get(itemname, {}),
                    args
                )
        return defaults

    @staticmethod
    def populate_parser(agroup):
        """
        Update passed argument group with backend options.

        The passed group is extended with backend options.

        Parameters
        ----------
        agroup : argparser.ArgumentGroup
            Argument group to store backend options.
        """
        agroup.add_argument('--moe-host', default='127.0.0.1',
                            help='MOE server host')
        agroup.add_argument('--moe-port', type=int, default=6543,
                            help='MOE server port')
        agroup.add_argument('--moe-covariance-type',
                            default='square_exponential',
                            help='covariance type')
        agroup.add_argument('--moe-hyperparameters', nargs='+',
                            help='covariance hyper parameters')

    @staticmethod
    def enabled():
        """
        Return backend enabling status.

        The backend is disabled if dependencies are not installed.

        Returns
        -------
        bool or str
            Returns ``True`` if backend is enabled and can be used, string
            with the disable reason otherwise.
        """
        if _enabled:
            return True
        return (
            'Install clientMOE package and scipy using `pip`:\n'
            'pip3 install \'clientmoe @ '
            'https://github.com/RokoMijic/clientMOE/archive/master.zip\' '
            'scipy'
            '\n\n'
            'The clientMOE package above is python3 update of the original '
            'clientMOE package available on PyPI.'
        )


from . import register_backend
register_backend('moe', MOEBackend, 'MOE optimization engine',
                 'MOE optimization engine requires running MOE server. Please '
                 'follow official instructions to setup MOE server: '
                 'http://yelp.github.io/MOE/install.html')
