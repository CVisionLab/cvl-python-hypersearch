# -*- coding: utf-8 -*-

"""Random search optimization backend."""

import numpy as np

from .abstract import AbstractBackend
from ..common import SamplePoint


class RandomBackend(AbstractBackend):
    """
    Random search optimization backend.

    Implements random uniform sampling over search space.
    """

    def __init__(self, search_space, args):
        """
        Construct random search backend instance.

        Parameters
        ----------
        search_space : dict
            Search space specification:
            ``{'param1': [min, max], 'param2': [min, max]}``.
        args : argparse.Namespace
            Command line parameters.
        """
        super(RandomBackend, self).__init__()
        self._search_space = search_space

    def get_suggestions(self, num_points=1):
        """
        Get next random suggestions.

        Parameters
        ----------
        num_points : int, optional
            Number of suggestions to return.

        Returns
        -------
        list
            List of suggestions. Each suggestion is a dict.
        """
        return [
            SamplePoint({name: np.random.uniform(*rng) for name, rng in
                         self._search_space.items()})
            for i in range(num_points)
        ]

    def add_observation(self, sample_point, objective, variance):
        """
        Add observation point.

        This function is a no-op for the random search backend.
        """
        pass

    @staticmethod
    def populate_parser(agroup):
        """
        Update passed argument group with backend options.

        Random search backend has no options. This function does nothing.
        """
        pass

    @staticmethod
    def enabled():
        """
        Return backend state: enabled or not.

        This backend is always enabled.

        Returns
        -------
        bool
            This backend is always enabled. Returns ``True``.
        """
        return True


from . import register_backend
register_backend('random', RandomBackend, 'Random search engine',
                 'Simple random search using uniform distribution')
