# -*- coding: UTF-8 -*-
"""
Sub-package with all backends implementations.

Maintains global list of registered backends.

Attributes
----------
Backend : namedtuple
    A named tuple with ``type``, ``title``, ``description`` and ``name``
    fields.
"""

import pkgutil
import importlib
import copy
from collections import namedtuple, OrderedDict

_BACKENDS = OrderedDict()


Backend = namedtuple('Backend', ['type', 'title', 'description', 'name'])
"""Optimization backend object."""


def register_backend(name, backendtype, title, description=None):
    """
    Register new optimization backend.

    Parameters
    ----------
    name : str
        Backend name.
    backendtype : AbstractBackend subclass
        A type of the backend (used to construct backend instance).
    title : str
        Backend title (displayed to the user)
    description : str, optional
        Backend description.
    """
    _BACKENDS[name] = Backend(backendtype, title, description, name)


def list_backends():
    """
    Return list of registered backends.

    The returned list is a deep copy of the internal list so any changes are
    made to the returned list do not have any effect.

    Returns
    -------
    list
        List of registered backends
    """
    return copy.deepcopy(_BACKENDS)


# Load all built-in backends
for loader, name, is_pkg in pkgutil.iter_modules(__path__):
    importlib.import_module('.' + name, __package__)
