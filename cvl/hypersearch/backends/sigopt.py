# -*- coding: utf-8 -*-

"""SigOpt hyper-parameters optimization backend."""

import collections as coll

from .abstract import AbstractBackend
from ..common import SamplePoint
try:
    import sigopt
except ImportError:
    _enabled = False
else:
    _enabled = True


class SigoptBackend(AbstractBackend):
    """
    SigOpt optimization backend.

    This backend uses paid service ``SigOpt`` to perform optimization and
    requires corresponding API tokens to work.
    """

    def __init__(self, search_space, args):
        """
        Create sigopt backend instance.

        Parameters
        ----------
        search_space : dict
            search space specification:
            ``{'param1': [min, max], 'param2': [min, max]}``
        args : argparse.Namespace
            command line parameters
        """
        super(SigoptBackend, self).__init__()
        self._search_space = search_space
        self._connection = sigopt.Connection(client_token=args.sigopt_token)
        experiment = self._connection.experiments().create(
            name=args.sigopt_experiment_name,
            parameters=[
                {
                    'name': name,
                    'type': 'double',
                    'bounds': {
                        'min': bounds[0],
                        'max': bounds[1]
                    }
                }
                for name, bounds in search_space.items()
            ]
        )
        self._experiment_id = experiment.id
        self.log('Created experiment: https://sigopt.com/experiment/'
                 '{0}\r\n'.format(self._experiment_id), 0)

    def get_suggestions(self, num_points=1):
        """
        Get next suggestions.

        Parameters
        ----------
        num_points : int, optional
            Number of suggestions to return

        Returns
        -------
        list
            List of suggestions. Each suggestion is a dict.
        """
        suggestions = []
        for i in range(num_points):
            suggestion = self._connection.experiments(self._experiment_id)\
                .suggestions().create()
            sample_point = SamplePoint(suggestion.assignments)
            sample_point.suggestion = suggestion.id
            suggestions.append(sample_point)
        return suggestions

    def add_observation(self, sample_point, objective, variance):
        """
        Add observation point.

        Parameters
        ----------
        sample_point : dict
            Sample point (point observation was sampled at)
        objective : float
            Observed objective value.
        variance : float
            Observed objective variance.
        """
        # Sigopt maximizes objective while cvl-hyper-search expects
        # minimization so we inverse objective sign
        if sample_point.suggestion is not None:
            self._connection.experiments(self._experiment_id).observations()\
                .create(
                    suggestion=sample_point.suggestion,
                    value=-1.0 * objective,
                    value_stddev=variance
            )
        else:
            self._connection.experiments(self._experiment_id).observations()\
                .create(
                    assignments=sample_point,
                    value=-1.0 * objective,
                    value_stddev=variance
            )

    def _setup_options(self, optgroups, defaults, args):
        """
        Set backend settings.

        Parameters
        ----------
        optgroups : dict
            Options definitions.
        defaults : dics
            Options defaults.
        args : argparse.Namespace
            Parse command line arguments.

        Returns
        -------
        dict
            Backend options mapping.
        """
        for itemname, conversion in optgroups.items():
            argname = 'moe_' + itemname
            if argname in args and isinstance(conversion, coll.Callable):
                defaults[itemname] = conversion(getattr(args, argname))
            elif isinstance(conversion, coll.Mapping):
                defaults[itemname] = self._setup_options(
                    optgroups[itemname],
                    defaults.get(itemname, {}),
                    args
                )
        return defaults

    @staticmethod
    def populate_parser(agroup):
        """
        Update passed argument group with backend options.

        The passed group is extended with backend options.

        Parameters
        ----------
        agroup : argparser.ArgumentGroup
            Argument group to store backend options.
        """
        agroup.add_argument('--sigopt-token', required=True,
                            help='Sigopt.com API token')
        agroup.add_argument('--sigopt-experiment-name',
                            default='cvl-hyper-search',
                            help='Sigopt experiment name')
        agroup.add_argument('--sigopt-experiment-id',
                            help='Do not create new experiment and use '
                                 'provided')

    @staticmethod
    def enabled():
        """
        Return backend enabling status.

        The backend is disabled if dependencies are not installed.

        Returns
        -------
        bool or str
            Returns ``True`` if backend is enabled and can be used, string
            with the disable reason otherwise.
        """
        if _enabled:
            return True
        return 'Install `sigopt` package via `pip`'


from . import register_backend
register_backend('sigopt', SigoptBackend, 'Sigopt.com optimization engine',
                 'Optimizes using Sigopt.com API. Requires active '
                 'subscription.\nPlease note that in Sigopt dashboard '
                 'objective values will have negative sign because sigopt '
                 'solves maximization problem while cvl-hyper-search solves '
                 'minimization problem.')
