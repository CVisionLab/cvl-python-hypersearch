# -*- coding: UTF-8 -*-

"""Container of DTerminals implementation."""


import urwid
from .dterm import DTerminal

# based on urwid-stackedwidget


class StackedDTerm(urwid.Widget):
    """A terminal container that presents one child term at a time."""

    def __init__(self):
        """Init terminal container instance."""
        self.terms = []
        self.current = 0
        self.render = self._first_render

    def add_term(self, term=None):
        """
        Append a term at the end of the list.

        If ``term`` is not passed, creates new `DTerminal` instance and appends
        to the list.

        Parameters
        ----------
        term : DTerminal, optional
            Terminal instance.
        """
        term = term or DTerminal()
        self.terms.append(term)

    def insert_term(self, index, term=None):
        """
        Insert a term at a given index.

        If ``term`` is not passed, creates new `DTerminal` instance and inserts
        in the list.

        Parameters
        ----------
        index : int
            List position of the new terminal.
        term : DTerminal, optional
            Terminal instance.
        """
        term = term or DTerminal()
        self.terms.insert(index, term)

    def pop_term(self):
        """
        Retrieve and remove the last term (with the maximum index).

        Returns
        -------
        DTerminal
            Popped terminal instance.
        """
        n = len(self.terms)
        assert n > 0

        term = self.terms.pop()
        if self.current == n - 1:
            self.current -= 1

        self._invalidate()

        return term

    def show_term(self, index):
        """
        Switch to the terminal with the given index.

        Parameters
        ----------
        index : int
            Index of the terminal to switch to.
        """
        assert 0 <= index < len(self.terms)
        self.current = index
        self._invalidate()

    def show_next_term(self):
        """Show a terminal next to the current index."""
        n = self.term_count
        self.show_term((self.current + 1) % n)

    def show_previous_term(self):
        """Show a terminal previous to the current index."""
        n = self.term_count
        self.show_term((self.current - 1 + n) % n)

    @property
    def term_count(self):
        """int: Current count of terminals in the list."""
        return len(self.terms)

    @property
    def current_term(self):
        """
        DTerminal: Current (active) terminal instance.

        ``None`` if no terminal was added.
        """
        if self.term_count > 0:
            return self.terms[self.current]
        else:
            return None

    @property
    def index(self):
        """int: Current terminal index."""
        return self.current

    def selectable(self):
        """
        Return interaction flag.

        Required by the underlying widget system (urwid).

        Always returns ``True`` to make it possible to get keyboard input.

        Returns
        -------
        Boolean
            Always ``True``.
        """

        return True

    def _render(self, size, focus=False):
        """
        Render widget.

        Parameters
        ----------
        size : tuple of int
            Widget size (width, height)
        focus : bool, optional
            Current focus state.
        """
        assert self.current_term is not None
        return self.current_term.render(size, focus)

    def keypress(self, size, key):
        """
        Handle keypress events.

        Passes key input to the current terminal. If the current term is
        ``None`` then it returns the given key input so that
        ``unhandled_input`` function can handle it.

        Parameters
        ----------
        size : tuple of int
            Widget size (width, height)
        key : str
            Pressed key.
        """

        if self.current_term is not None:
            return self.current_term.keypress(size, key)
        else:
            return key

    def mouse_event(self, size, event, button, col, row, focus):
        """
        Handle mouse events.

        Passes events to the current terminal. If the current term is
        ``None`` then it returns ``False`` to signal the event was not handled.

        Parameters
        ----------
        size : tuple of int
            Widget size (width, height)
        event : str
            Event type
        button : ?
            Pressed buttons
        col : int
            Event position: column.
        row : int
            Event position: row.
        focus : Bool
            Current focus state
        """

        if self.current_term is not None:
            return self.current_term.mouse_event(
                size, event, button, col, row, focus)
        else:
            return False

    def feed(self, data, termnum):
        """
        Add output to the terminal given by index.

        Parameters
        ----------
        data : str
            Output line to add to the terminal.
        termnum : int
            Terminal index.
        """
        assert 0 <= termnum < self.term_count, 'Bad terminal number'
        self.terms[termnum].feed(data)

    def clear(self, termnum):
        """
        Clear terminal given by index.

        Parameters
        ----------
        termnum : int
            Terminal index.
        """
        assert 0 <= termnum < self.term_count, 'Bad terminal number'
        self.terms[termnum].clear()

    def _first_render(self, size, focus=False):
        """
        Handle the first widget rendering.

        The first rendering is special as we need to render all child
        terminals.

        Parameters
        ----------
        size : tuple of int
            Widget size (width, height).
        focus : bool, optional
            Current focus state.
        """
        self.render = self._render
        for term in self.terms:
            term.touch_term(*size)
        return self._render(size, focus)
