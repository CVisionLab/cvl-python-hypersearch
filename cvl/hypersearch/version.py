"""Helper module to get package version and distribution."""

import pkg_resources as pkr

try:
    __distribution__ = pkr.get_distribution('cvl-hypersearch')
    __version__ = __distribution__.version
except pkr.DistributionNotFound:
    # package is not installed
    __distribution__ = pkr.Distribution()
    __version__ = '0.0.0 (source)'
